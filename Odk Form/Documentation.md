# ODK FORM DOCUMENTATION
***

## Installation
***

* Before installing Central on your server, you need to install Docker

* Run the following command to uninstall all conflicting packages:

```
 for pkg in docker.io docker-doc docker-compose docker-compose-v2 podman-docker containerd runc; do sudo apt-get remove $pkg; done
```

* Set up Docker's apt repository.

```
sudo apt-get update
sudo apt-get install ca-certificates curl
sudo install -m 0755 -d /etc/apt/keyrings
sudo curl -fsSL https://download.docker.com/linux/ubuntu/gpg -o /etc/apt/keyrings/docker.asc
sudo chmod a+r /etc/apt/keyrings/docker.asc
```

* Add the repository to Apt sources

```
echo \
  "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.asc] https://download.docker.com/linux/ubuntu \
  $(. /etc/os-release && echo "$VERSION_CODENAME") stable" | \
  sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
sudo apt-get update
```

* To install the latest version, run:

```
sudo apt-get install docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin
```

* Verify that the Docker Engine installation is successful by running the hello-world image.

```
sudo docker run hello-world
```

* You have successfully downloaded Docker Engine

* Now lets get started setting up ODK Central

* Download the software. In the server window, type:


```
git clone https://github.com/getodk/central
```

* Go into the new central folder:

```
cd central
```

* Get the latest client and server:

```
git submodule update -i
```

* Update settings. First, copy the settings template file so you can edit it:

```
cp .env.template .env
```

* Launch the nano text editing application and specify required settings:

```
nano .env
```

* Inside the .env file set the DOMAIN to local and SSL_TYPE to selfsign.

* Save and exit from the file

* Let the system know that you want the latest version of the database:

```
touch ./files/allow-postgres14-upgrade
```
* Bundle everything together into a server. This will take a long time and generate quite a lot of text output. Don't worry if it seems to pause without saying anything for a while.

```
docker compose build
```

* Start the server software. The first time you start it, it will take a while to set itself up.

```
docker compose up -d
```
* See whether ODK has finished loading.

```
docker compose ps
```

* Now we need to need the set the username and password for logging in

* Ensure that you are in the central folder on your server. If you have not closed your console session from earlier, you should be fine. If you have just logged back into it:

```
cd central
```

* Create a new account. Make sure to substitute the email address that you want to use for this account.

```
docker compose exec service odk-cmd --email YOUREMAIL@ADDRESSHERE.com user-create
```

* Make the new account an administrator.

```
docker compose exec service odk-cmd --email YOUREMAIL@ADDRESSHERE.com user-promote
```

* Log into the Central website. Go to your domain name which is "localhost:80" and enter in your new credentials. Once you have one administrator account, you do not have to go through this process again for future accounts: you can log into the website with your new account, and directly create new users.

* If you ever lose track of your password, you can reset it with

```
docker compose exec service odk-cmd --email YOUREMAIL@ADDRESSHERE.com user-set-password
```

