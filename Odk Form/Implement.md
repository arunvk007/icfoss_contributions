# How ODK Works?
***

* Open a web browser and navigate to http://localhost:80. A login page will appear as follows:
![alt text](https://gitlab.com/arunvk007/icfoss_contributions/-/raw/main/Odk%20Form/images/img1.png)

* Type your login credentials and click Login

* After logging in, a home page will appears as follow:
![alt text](https://gitlab.com/arunvk007/icfoss_contributions/-/raw/main/Odk%20Form/images/img2.png)

* Click 'New' to create a new project

* Then you need to enter a name for your project
![alt text](https://gitlab.com/arunvk007/icfoss_contributions/-/raw/main/Odk%20Form/images/img3.png)

* After that click 'Create', Then you will be redirected to a new page as below:
![alt text](https://gitlab.com/arunvk007/icfoss_contributions/-/raw/main/Odk%20Form/images/img4.png)

* Click 'New' to create a new Form.
![alt text](https://gitlab.com/arunvk007/icfoss_contributions/-/raw/main/Odk%20Form/images/img5.png)

* I have attached the XML file that I used to create an ODK form for Akshaya Centre.