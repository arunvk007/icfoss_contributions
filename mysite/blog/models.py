from django.db import models

# Create your models here.
class MyModel(models.Model):
    name = models.CharField(max_length=250)
    description = models.TextField()
    
    def get_full_info(self):
        return f"{self.name}: {self.description}"