### Client-Server Architecture
***

* Client-server architecture is a common model for designing and organizing computer networks and software systems. 
* There are two main components: clients and servers
* Clients : Clients are devices or software applications that makes request for services or resources.
* Servers : Servers recieves requests from clients, process those requests, and send back the results.

### URL
***

* Uniform Resource Locator
* Its like an address for things on the internet.
* Its how we find and access web pages, documents, images, videos and other resources on the world wide web.
* A URL typically consists of several parts:
    
    * Protocol: This is how your web browser should communicate with the resource. Common protocols includes 'http' for regular web pages and 'https' for secure, encrypted web pages.

    * Domain: This is the web address of the server where the resource is located. Its often website name like "google.com."

    * Port: Sometimes you will see a colon followed by a number (eg; ':80' or ':443'). This specifies the network port used to connect to the server. Usually the browser assumes a default.

    * Path: This is the file path of the server. It the tells the server where to find the specific resource you are looking for.

    * Query: Sometimes yyou see a question mark followed by an additional information. This is used for passing data to the server, often used in search queries.

    * Fragment: An optional part of a URl, often indicated by a hash(#), that points to specify a specific section in a web page.

### URL Structure
***

Here's an example of a URL in action:

* URL : https://www.example.com:8080/path/to/resource?search=example#section2
* Protocol : "https://"
* Domain: "www.example.com"
* Port: ":8080"
* Path: "/path/to/resource"
* Query: "?search=example"
* Fragment: "#section2"

### URI
***

* Uniform Resource Identifier
* Its a simple way to point resources like web pages, documents, images or any kind of data.

### URL vs URI
***

* All URLs are URIs, but not all URIs are URLs.
* A URL is a type of URI that includes the information needed to locate and access a resource.
* Where URI is a broader term that covers any unique identifier for resources.





