# Django Middleware
***

* In django Middleware is a way to process requests and response globally before they reach the view or after they leave the view.
* They can perform tasks such as modifying requests or response objects, logging information, handling authentication and more.

* Middleware classes are defined in the 'MIDDLEWARE' setting in your django project's settings.py.
* The order in which Middleware classes are listed in 'MIDDLEWARE' setting matters, as they are executed in the order as they appears.


##### Heres a simple example for middleware class
```

# myapp/middleware.py

class MyMiddleware:
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        # Code to be executed for each request before the view is called
        print("This is executed before the view.")
        
        # Pass the request to the next middleware or view
        response = self.get_response(request)

        # Code to be executed for each request/response after the view is called
        print("This is executed after the view.")
        
        return response

```

* To use this middleware, you need to add its path to the MIDDLEWARE setting in your settings.py:

```
# settings.py

MIDDLEWARE = [
    # Other middleware classes...
    'myapp.middleware.MyMiddleware',
    # Other middleware classes...
]

```
