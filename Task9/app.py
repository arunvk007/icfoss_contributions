from flask import Flask, render_template

import requests
import datetime 

app = Flask(__name__)

chirpstack_email = ""  # Replace with your Chirpstack email
chirpstack_password = ""  # Replace with your Chirpstack password
chirpstack_url = ""  # Replace with your Chirpstack server URL

@app.route('/')
def index():

    # url = "https://lorawandev.icfoss.org"
    # token = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJhcyIsImV4cCI6MTY5ODQ4Njg4NywiaWQiOjcsImlzcyI6ImFzIiwibmJmIjoxNjk4NDAwNDg3LCJzdWIiOiJ1c2VyIiwidXNlcm5hbWUiOiJhcnVudmtAaWNmb3NzLm9yZyJ9.pABdwnO5l30Ie5ql4RgeLs3follmmSzUv8uhTcCLJ70" 
    
    # data =  get_applications_info(url,token)
    # print(data)
    return render_template('index.html')

# Function to get device information
# API - /api/applications

@app.route('/api/chirpstack-data')
def get_applications_info(): 
    
    url = "" #Replace with your sever URL
    token = "" #Replace with your authentication token
    

    # Set headers with authentication token
    headers = {
        "Accept": "application/json",
        "Grpc-Metadata-Authorization": "Bearer " + token
    }
    # Make a GET request to fetch applications information
    response = requests.get(url + "/api/applications?limit=5", headers=headers, stream=False)
    print(response)
    #Parse the JSON response and extract device details
    data = response.json()
    response = [app['name']for app in data['result']]   
    return response

# Function to get JWT token for authentication
# API - /api/internal/login

def apilogin(email, password, url):
    url = url + '/api/internal/login'
    credentials = '{"password": "' + password + '","email": "' + email + '"}'
    headers = {'content-type': 'application/json', 'Accept-Charset': 'UTF-8'}
    # Make a POST request to obtain the JWT token
    response = requests.post(url, data=credentials, headers=headers)
    data = response.json()
    print(data)
    return data['jwt']

# Main function
def main():
      
    # Get the JWT token for authentication
    token = apilogin(chirpstack_email, chirpstack_password, chirpstack_url)
    
    # Fetch and print device information
    applications_info = get_applications_info(chirpstack_url, token)
    print(applications_info)
    
    # Fetch and print application names
    response = get_applications_info(chirpstack_url,token)
    
    for name in response:
        print(name)   
        return render_template('index.html',data=name)
           
     
# if __name__ == '__main__': 
#     main()



if __name__ == '__main__':
    # main()
    app.run(debug=True)
