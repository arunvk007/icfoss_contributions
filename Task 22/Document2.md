### Chirpstack
***

* Open-source.
* Its a LoRaWan network server.
* Enables communication between LoRaWan devices and applications.
* It consists of :
    * Network Server
    * Gateway Bridge
    * Application Server


* LoRaWan is a network Layer protocol.
* Maintained by LoRa alliance (LoRa alliance provides LoRaWan certification for end devices).
* LoRaWan provides data communication over wireless LoRa networks.

LoRa Gateway:
* Transmits sensor data from an electrical device to cloud.

![IMAGE_DESCRIPTION](https://b2600043.smushcdn.com/2600043/wp-content/uploads/2023/08/2-768x354.png?lossy=0&strip=1&webp=1)

1. Chirpstack Concentratord:
* Handles data transmission from nodes(devices) to gateways.

2. Chirpstack Gateway Bridge:
* Acts as an intermediary, facilitating seamless communication between gateway harware and network server.

3. Chirpstack Network Server:
* Manages the entire network.
* Responsible for handling LoRaWan protocol
* Includes activation of devices.
* Data routing and communication to nodes.
* ensuring the security of the communication.

4. Chirpstack Application Server:
* Responsible for processing and interpreting the data recieved from end devices(nodes).
* Also performs decoding the data, storing it and triggering specific actions based on recieved information.






