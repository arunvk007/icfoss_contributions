from django.shortcuts import render
from .forms import myForm

# Create your views here.

def my_view(request):
    if request.method == 'POST':
        form = myForm(request.POST)
        if form.is_valid():
            name = form.cleaned_data['name']
            email = form.cleaned_data['email']
            message = form.cleaned_data['message']
        else:
            pass
        
    else:
        form = myForm()
        
    return render(request, 'my_template.html', {'form': form})        