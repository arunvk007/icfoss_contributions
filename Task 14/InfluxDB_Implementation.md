### InfluxDB Implementation
***

* To start InfluxDB, you can use the following command

   $ sudo service influxdb start

* To access InfluxDB

   $ influx

* To create a database

   $ CREATE DATABASE mydb

* To switch to a specific database

   $ use mydb

* To list all the dabases

   $ SHOW DATABASES

* To list all tables in the selected database

   $ SHOW MEASUREMENTS

* You can write data using the 'insert' statement

   $ INSERT measurement,tag1=value,tage2=value field=value

   eg: <br>

   INSERT cricket,name="Dhoni" Runs=12237,Centuries=12,Fifties=68

   Never use "spaces" in between the commands
   
   Here, cricket is the measurement(table name). After that we put a coma which indicates that the influx is expecting a tag and its corresponding value. Then we put a space to add field names and its values.

   String values should be put in double quotes.

* To retrieve all the data from database

   $ SELECT * FROM measurement

   eg: <br>

   SELECT * FROM cricket

* To delete a database

   $ DROP DATABASE mydb












